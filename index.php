<!DOCTYPE html>
<html>
<head>
	<title>EjercicioWeb3</title>
</head>
<body>
	<h2>Factorial</h2>
	<form>
		<input type="number" name="a" placeholder="capture número" min="1">
		<button type="submit">Calcular</button>
	</form>
	<?php if(isset($_GET['a']) && $_GET['a']>0): ?>
	<?php 
		$a = $_GET['a'];

		function calcularFactorial($numero){ 
		    $factorial = 1; 
		    for ($i = 1; $i <= $numero; $i++){ 
		      $factorial = $factorial * $i; 
		    } 
		    return $factorial; 
		} 

		function descomponerFactorial($numero){ 
		    for ($i = $numero; $i > 0; $i--){ 
		    	if ($numero==$i) 
		    		$resultado = $i;
		    	else
		    		$resultado .= " * ".$i; 
		    } 
		    return $resultado; 
		}

		$fact = calcularFactorial($a);
	?>
	<ul>
		<li>
			Mensaje: El factorial de <?php echo $a; ?>
			es igual a <?php echo $fact; ?>
		</li>
		<li>
			Calculo del factorial de <?php echo $a; ?>
			<ul>
				<li><?php echo $a."! = ".descomponerFactorial($a); ?></li>
				<li><?php echo $a."! = ".$fact; ?></li>
			</ul>
		</li>
	</ul>
	<?php endif; ?>
</body>
</html>